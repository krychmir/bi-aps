module compare(

    input [31:0] a,
    input [31:0] b,

    output reg out
    
);

    always @(a or b)
    begin
        if(a == b) out = 1;
        else out = 0;
    end

endmodule