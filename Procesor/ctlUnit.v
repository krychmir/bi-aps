module mainDec(

    input [5:0] opcode,

    output regWrite,
    output regDst,
    output aluSrc,
    output [1:0] aluOP,
    output branch,
    output memWrite,
    output memToReg,
    output PCSrcJal,
    output PCSrcJr

);

    reg [9:0] outputs; // regWrite, regDst, aluSrc, [1:0] aluOP, branch, memWrite, memToReg, PCSrcJal, PCSrcJr

    always @(opcode)
    begin
        case(opcode)

            'b000000:   outputs = 'b1101000000; //R TYP
            'b100011:   outputs = 'b1010000100; //lw
            'b101011:   outputs = 'b0X10001X00; //sw
            'b000100:   outputs = 'b0X00110X00; //beq
            'b001000:   outputs = 'b1010000000; //addi
            'b000011:   outputs = 'b1XXXXX0X10; //jal
            'b000111:   outputs = 'b0XXXXX0X01; //jr
            'b011111:   outputs = 'b1101100000; //addu
            default:    outputs = 'b0000000000; //ERROR

        endcase
    end

    assign regWrite     = outputs[9];
    assign regDst       = outputs[8];
    assign aluSrc       = outputs[7];
    assign aluOP[1:0]   = outputs[6:5];
    assign branch       = outputs[4];
    assign memWrite     = outputs[3];
    assign memToReg     = outputs[2];
    assign PCSrcJal     = outputs[1];
    assign PCSrcJr      = outputs[0];

endmodule

module aluDec(

    input [1:0] ALUOp,
    input [5:0] funct,
    input [4:0] shamt,

    output reg [3:0] ALUControl

);
    
    always @(ALUOp or funct or shamt)
    begin
        case(ALUOp)

            'b00:       ALUControl = 'b0010; //Addition
            'b01:       ALUControl = 'b0110; //Subtraction

            'b10:       begin
                            case(funct)

                                'b100000:   ALUControl = 'b0010; //Additon
                                'b100010:   ALUControl = 'b0110; //Subtraction
                                'b100100:   ALUControl = 'b0000; //And
                                'b100101:   ALUControl = 'b0001; //or
                                'b101010:   ALUControl = 'b0111; //set less than

                            endcase
                        end

            'b11:       begin
                            case(funct)
                                
                                'b010000:   begin
                                                case(shamt)

                                                    'b00000:    ALUControl = 'b1000; //Additon byte after byte
                                                    'b00100:    ALUControl = 'b1001; //saturated addition byte after byte

                                                endcase
                                            end
                            endcase   
                        end     

            default:    ALUControl = 'b1111; //ERROR
            
        endcase
    end

endmodule


module ctlUnit(

    input [5:0] opcode,
    input [5:0] funct,
    input [4:0] shamt,

    output regWrite,
    output regDst,
    output aluSrc,
    output branch,
    output memWrite,
    output memToReg,
    output PCSrcJal,
    output PCSrcJr,
    output [3:0] ALUControl

);

    wire [1:0] aluOP_wire;

    mainDec mainDec_mod(opcode, regWrite, regDst, aluSrc, aluOP_wire, branch, memWrite, memToReg, PCSrcJal, PCSrcJr);
    aluDec aluDec_mod(aluOP_wire, funct, shamt, ALUControl);
    
endmodule