module adder_TB();

    reg [31:0] a;
    reg [31:0] b;
    reg c0;

    wire [31:0] sum;
    wire c32;

adder adder_DUT (a, b, c0, sum, c32);

initial begin
    
    $dumpfile("test");
    $dumpvars;

    a = 0;
    b = 0;
    c0 = 0;

     
    #2097152 $finish;
end

    always #1 c0 = ~c0;
    always #2 a = a + 7;
    always #4 b = b + 13;
    always #8 b = b + 3;
    always #16 a = a + 2;
    always #1024 a += a;
    always #1024 b += b;

    always @(sum or c32) 
    begin
        if(a + b + c0 != {c32, sum}) 
        begin
            $display("Fail!, %d + %d + %d != %d + %d | (a + b + c0 != {c32, sum})", a, b, c0, sum, c32);
            $finish;
        end
    end

endmodule