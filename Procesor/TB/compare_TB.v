module compare_TB();

reg [31:0] a;
reg [31:0] b;
wire out;

compare compare_DUT(a, b, out);

initial begin
    
    $dumpfile("test");
    $dumpvars;

    a = 0;
    b = 0;

    #1024 $finish;

end

always #2 a = $urandom%100;
always #2 b = $urandom%100;

always @(out) 
begin
    if(a == b && out != 1 || a != b && out == 1) 
    begin
        $display("Fail!, %d == %d && %d != 1 || %d != %d && %d == 1 | (a == b && out != 1 || a != b && out == 1)", a, b, out, a, b, out);
        $finish;
    end
end


endmodule