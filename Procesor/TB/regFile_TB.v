module regFile_TB();

    reg [4:0] A1;
    reg [4:0] A2;
    reg [4:0] A3;
    reg [31:0] WD3;
    reg WE3;
    reg clk;
    
    wire [31:0] RD1;
    wire [31:0] RD2;

    regFile regFile_DUT(A1, A2, A3, WD3, WE3, clk, RD1, RD2);

    initial begin
    
        $dumpfile("test");
        $dumpvars;
        clk = 0;
        WE3 = 0;

        #1;

        for(A3 = 0; A3 < 31; A3 = A3 + 1) //Testování WE = 0 => Nemělo by do jít k zapsání.
        begin
            WD3 = A3;
            #1;
        end

        WE3 = 1;

        for(A3 = 0; A3 < 31; A3 = A3 + 1) //Testování WE = 1 => Mělo by do jít k zapsání.
        begin
            WD3 = A3;
            #1;
        end

        WE3 = 0;

        for(A1 = 0; A1 < 30; A1 = A1 + 2) //Čtení registrů
        begin
            A2 = A1 + 1;
            #1;
        end


        #1024 $finish;

    end

    always #1 clk = !clk;


endmodule