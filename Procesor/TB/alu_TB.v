module alu_TB();

reg [31:0] A;
reg [31:0] B;
reg [3:0] control;

wire zero;
wire [31:0] result;


alu alu_DUT(A, B, control, zero, result);

initial begin
    
    $dumpfile("test");
    $dumpvars;

    A = 'hFF_C4_3C_02;
    B = 'hFF_3C_3C_21;
    control = 'b1001;
    #1;
    control = 'b1000;

    #1024 $finish;

end



endmodule