module mux2_1_TB();

    reg [31:0] d0;
    reg [31:0] d1;
    reg select;

    wire [31:0] y;

mux2_1 mux (d0, d1, select, y);

initial begin

    $dumpfile("test");
    $dumpvars;

    d0 = 17;
    d1 = 8;
    select = 0;
    #1
    select = 1;
    #1
    d0 = 9;
    d1 = 25;
    #1
    select = 0;
    #1 $finish;

end


endmodule