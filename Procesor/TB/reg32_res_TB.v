module reg32_res_TB();

reg [31:0] in;
reg clk;
reg reset;

wire [31:0] out;

reg32_res reg32_res_DUT(in, reset, clk, out);

initial begin

    $dumpfile("test");
    $dumpvars;
    clk = 0;
    in = 0;
    reset = 0;

    #1024 $finish;

end

always #1 clk = !clk;
always #4 in = in + $urandom%100;
always #10 begin
    reset = 1;
    #1;
    reset = 0;
end

endmodule