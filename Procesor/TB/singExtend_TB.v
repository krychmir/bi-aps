module singExtend_TB();

reg unsigned [15:0] in;
wire unsigned [31:0] out;

signExtend singExtend_DUT(in, out);

initial begin
    
    $dumpfile("test");
    $dumpvars;

    in = 0;

    #1048576 $finish;

end

always #2 in = $urandom%65536;

always @(out) 
begin
    if(in != out[15:0] || {16{in[15]}} != out[31:16]) 
    begin
        $display("%b", out);
        $display("Fail!, %b != %b || %b != %b | (in != out[15:0] || {16{in[15]}} != out[31:16])", in, out[15:0], {16{in[15]}}, out[31:16]);
        $finish;
    end
end


endmodule