module cltUnit_TB();

    reg [5:0] opcode;
    reg [5:0] funct;
    reg [4:0] shamt;

    wire regWrite;
    wire regDst;
    wire aluSrc;
    wire branch;
    wire memWrite;
    wire memToReg;
    wire PCSrcJal;
    wire PCSrcJr;
    wire [3:0] ALUControl;

    ctlUnit ctlUnit_DUT(opcode, funct, shamt, regWrite, regDst, aluSrc, branch, memWrite, memToReg, PCSrcJal, PCSrcJr, ALUControl);

    initial begin
    
        $dumpfile("test");
        $dumpvars;

        //-----------------------------------------------------------

        opcode = 'b000000; //TYP R TEST
        #1;
        if(
            regWrite    != 'b1 ||
            regDst      != 'b1 ||
            aluSrc      != 'b0 ||
            branch      != 'b0 ||
            memWrite    != 'b0 ||
            memToReg    != 'b0 ||
            PCSrcJal    != 'b0 ||
            PCSrcJr     != 'b0
        )
        begin
            $display("R TYP FAIL");
            $finish;
        end

        funct = 'b100000; //TYP R TEST ADD
        #1
        if(ALUControl != 'b0010)
        begin
            $display("R TYP FAIL/ADD");
            $finish;
        end

        funct = 'b100010; //TYP R TEST SUB
        #1
        if(ALUControl != 'b0110)
        begin
            $display("R TYP FAIL/SUB");
            $finish;
        end

        funct = 'b100100; //TYP R TEST AND
        #1
        if(ALUControl != 'b0000)
        begin
            $display("R TYP FAIL/AND");
            $finish;
        end

        funct = 'b100101; //TYP R TEST OR
        #1
        if(ALUControl != 'b0001)
        begin
            $display("R TYP FAIL/OR");
            $finish;
        end

        funct = 'b101010; //TYP R TEST STL
        #1
        if(ALUControl != 'b0111)
        begin
            $display("R TYP FAIL/STL");
            $finish;
        end

        //-----------------------------------------------------------

        opcode = 'b100011; //LW TEST
        #1;
        if(
            regWrite    != 'b1 ||
            regDst      != 'b0 ||
            aluSrc      != 'b1 ||
            branch      != 'b0 ||
            memWrite    != 'b0 ||
            memToReg    != 'b1 ||
            PCSrcJal    != 'b0 ||
            PCSrcJr     != 'b0
        )
        begin
            $display("LW TEST FAIL");
            $finish;
        end

        if(ALUControl != 'b0010)
        begin
            $display("LW TEST FAIL/ADD");
            $finish;
        end

        //-----------------------------------------------------------

        opcode = 'b101011; //SW TEST
        #1;
        if(
            regWrite    != 'b0 ||
            aluSrc      != 'b1 ||
            branch      != 'b0 ||
            memWrite    != 'b1 ||
            PCSrcJal    != 'b0 ||
            PCSrcJr     != 'b0
        )
        begin
            $display("SW TEST FAIL");
            $finish;
        end

        if(ALUControl != 'b0010)
        begin
            $display("SW TEST FAIL/ADD");
            $finish;
        end

        //-----------------------------------------------------------

        opcode = 'b000100; //BEQ TEST
        #1;
        if(
            regWrite    != 'b0 ||
            aluSrc      != 'b0 ||
            branch      != 'b1 ||
            memWrite    != 'b0 ||
            PCSrcJal    != 'b0 ||
            PCSrcJr     != 'b0
        )
        begin
            $display("BEQ TEST FAIL");
            $finish;
        end

        if(ALUControl != 'b0110)
        begin
            $display("BEQ TEST FAIL/SUB");
            $finish;
        end

        //-----------------------------------------------------------

        opcode = 'b001000; //ADDI TEST
        #1;
        if(
            regWrite    != 'b1 ||
            regDst      != 'b0 ||
            aluSrc      != 'b1 ||
            branch      != 'b0 ||
            memWrite    != 'b0 ||
            memToReg    != 'b0 ||
            PCSrcJal    != 'b0 ||
            PCSrcJr     != 'b0
        )
        begin
            $display("ADDI TEST FAIL");
            $finish;
        end

        if(ALUControl != 'b0010)
        begin
            $display("ADDI TEST FAIL/ADD");
            $finish;
        end

        //-----------------------------------------------------------

        opcode = 'b000011; //JAL TEST
        #1;
        if(
            regWrite    != 'b1 ||
            memWrite    != 'b0 ||
            PCSrcJal    != 'b1 ||
            PCSrcJr     != 'b0
        )
        begin
            $display("JAL TEST FAIL");
            $finish;
        end

        //-----------------------------------------------------------

        opcode = 'b000111; //JR TEST
        #1;
        if(
            regWrite    != 'b0 ||
            memWrite    != 'b0 ||
            PCSrcJal    != 'b0 ||
            PCSrcJr     != 'b1
        )
        begin
            $display("JR TEST FAIL");
            $finish;
        end

        //-----------------------------------------------------------

        opcode = 'b011111; //addu[_s].qb TEST
        funct = 'b010000;

        shamt = 'b00000; //addition byte after byte

        #1;
        if(
            regWrite    != 'b1 ||
            regDst      != 'b1 ||
            aluSrc      != 'b0 ||
            branch      != 'b0 ||
            memWrite    != 'b0 ||
            memToReg    != 'b0 ||
            PCSrcJal    != 'b0 ||
            PCSrcJr     != 'b0
        )
        begin
            $display("addu[_s].qb TEST FAIL");
            $finish;
        end

        if(ALUControl != 'b1000)
        begin
            $display("addu[_s].qb TEST FAIL/addition byte after byte");
            $finish;
        end

        shamt = 'b00100; //saturted addition byte after byte

        #1;
        if(ALUControl != 'b1001)
        begin
            $display("addu[_s].qb TEST FAIL/saturted addition byte after byte");
            $finish;
        end

        //-----------------------------------------------------------


        #1;
        $display("KONEC TESTU. VŠE OK");
        $finish;

    end

endmodule