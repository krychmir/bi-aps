module mux4_1_TP();

reg [31:0] d0;
reg [31:0] d1;
reg [31:0] d2;
reg [31:0] d3;
reg [1:0] select;

wire [31:0] y;

mux4_1 mux (d0, d1, d2, d3, select, y);

initial begin
    
    $dumpfile("test");
    $dumpvars;

    d0 = 5;
    d1 = 98;
    d2 = 66;
    d3 = 42;
    select = 0;
    #1
    d0 = 1;
    #1
    select = 1;
    #1
    d1 = 1;
    #1
    select = 2;
    #1
    d2 = 1;
    #1
    select = 3;
    #1
    d3 = 1;
    #1 $finish;

end

endmodule