module reg32_TB();

reg [31:0] in;
reg clk;

wire [31:0] out;

reg32 reg32_DUT(in, clk, out);

initial begin

    $dumpfile("test");
    $dumpvars;
    clk = 0;
    in = 0;

    #1024 $finish;

end

always #1 clk = !clk;
always #4 in = in + $urandom%100;

endmodule