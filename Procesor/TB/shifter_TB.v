module shifter_TB();

reg [31:0] a;
wire [31:0] b;

shifter shiftet_DUT(a,b);

initial begin

    $dumpfile("test");
    $dumpvars;

    a = 0;    
    #2097152 $finish;

end

always #2 a = $urandom%1073741824;

always @(b) 
    begin
        if(4 * a !=  b) 
        begin
            $display("Fail!, 4 * %d != %d | (4 * a !=  b)", a, b);
            $finish;
        end
    end

endmodule