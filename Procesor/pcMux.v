module pcMux(

    input [31:0] jr,
    input [31:0] jal, 
    input [31:0] beq,
    input [31:0] zero, 

    input [0:0] pcSrcBeq,
    input [0:0] pcSrcJr,
    input [0:0] pcSrcJal,

    output reg [31:0] y
    
);
  
    always @(jr or jal or beq or zero or pcSrcBeq or pcSrcJr or pcSrcJal)
    begin
        
        if(jr == 1) y = pcSrcJr;
        else if (jal == 1) y = pcSrcJal;
        else if (beq == 1) y = pcSrcBeq;
        else y = zero;

    end

endmodule