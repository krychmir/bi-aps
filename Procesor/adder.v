module adder(
    
    input [31:0] a,
    input [31:0] b,
    input c0,

    output reg [31:0] sum,
    output reg c32
    
);

    always @(a or b or c0)
    begin
        {c32, sum} = a + b + c0;
    end


endmodule