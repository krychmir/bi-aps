module mux4_1(

    input [31:0] d0,
    input [31:0] d1, 
    input [31:0] d2,
    input [31:0] d3, 
    input [1:0] select,

    output reg [31:0] y
    
);
  
    always @(select or d0 or d1 or d2 or d3)
    begin
        case(select)
            0:          y = d0;
            1:          y = d1;
            2:          y = d2;
            default:    y = d3;
        endcase
    end

endmodule