module reg32_res(

    input [31:0] datain,
    input reset,
    input clk,

    output reg [31:0] dataout

);

    always @(posedge clk or reset)
    begin
        if(reset == 1)
            dataout = 0;
        else
            dataout = datain;
    end

endmodule