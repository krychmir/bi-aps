module regFile(

    input [4:0] A1,
    input [4:0] A2,
    input [4:0] A3,
    input [31:0] WD3,
    input WE3,
    input clk,
    
    output reg [31:0] RD1,
    output reg [31:0] RD2

);

    reg [31:0] rf[31:0];

    always @(A1) //Výpis registrů pomocí A1
    begin
        if(A1 == 0)
            RD1 = 'h0000_0000;
        else
            RD1 = rf[A1];
    end

    always @(A2) //Výpis registrů pomocí A2
    begin
        if(A2 == 0)
            RD2 = 'h0000_0000;
        else
            RD2 = rf[A2];
    end

    always @(posedge clk)
    begin
        if(WE3 == 1)
            rf[A3] = WD3;
        else ;
    end
    
endmodule