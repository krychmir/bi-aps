module processor( input         clk, reset,
                  output [31:0] PC,
                  input  [31:0] instruction,
                  output        WE,
                  output [31:0] address_to_mem,
                  output [31:0] data_to_mem,
                  input  [31:0] data_from_mem
                );
    
    wire [31:0] instr;
    wire [31:0] pc;
    wire [31:0] signImm;
    wire [31:0] pcNext;
    wire [31:0] srcA;
    wire [31:0] srcB;
    wire [31:0] aluOut;
    wire [31:0] writeData;
    wire [31:0] readData;
    wire [31:0] pcBranch;
    wire [31:0] pcPlus4;
    wire [31:0] result;
    wire [31:0] pcJal;
    wire [31:0] writeData3;
    wire [31:0] always4;

    wire [ 4:0] writeReg;
    wire [ 4:0] addes3;
    wire [ 4:0] always31;

    wire [ 3:0] aluControl;

    wire [ 0:0] zero;
    wire [ 0:0] branch;
    wire [ 0:0] pcSrcBeq;
    wire [ 0:0] regDst;
    wire [ 0:0] aluSrc;
    wire [ 0:0] memWrite;
    wire [ 0:0] memToReg;
    wire [ 0:0] regWrite;
    wire [ 0:0] pcSrcJr;
    wire [ 0:0] pcSrcJal;
    wire [ 0:0] gnd;

    ctlUnit ControlUnit(    instr[31:26], 
                            instr[5:0], 
                            instr[10:6],

                            regWrite, 
                            regDst, 
                            aluSrc, 
                            branch,
                            memWrite,
                            memToReg,
                            pcSrcJal,
                            pcSrcJr,
                            aluControl
                        );
    
    pcMux PCMux(    srcA, 
                    pcJal, 
                    pcBranch, 
                    pcPlus4, 
                    pcSrcBeq, 
                    pcSrcJr, 
                    pcSrcJal,

                    pcNext
                );

    reg32_res ProgramCounter(   pcNext, 
                                reset, 
                                clk, 
                                pc
                            );

    

endmodule

//---------------------------------------------------------------------- MUX2_1

module mux2_1(

    input [31:0] d0,
    input [31:0] d1, 
    input select,

    output reg [31:0] y
    
);

    always @(select or d0 or d1)
    begin
        if(select == 0)
            y = d0;
        else
            y = d1;
    end

endmodule

//---------------------------------------------------------------------- MUX4_1

module mux4_1(

    input [31:0] d0,
    input [31:0] d1, 
    input [31:0] d2,
    input [31:0] d3, 
    input [1:0] select,

    output reg [31:0] y
    
);
  
    always @(select or d0 or d1 or d2 or d3)
    begin
        case(select)
            0:          y = d0;
            1:          y = d1;
            2:          y = d2;
            default:    y = d3;
        endcase
    end

endmodule

//---------------------------------------------------------------------- ADDER

module adder(
    
    input [31:0] a,
    input [31:0] b,
    input c0,

    output reg [31:0] sum,
    output reg c32
    
);

    always @(a or b or c0)
    begin
        {c32, sum} = a + b + c0;
    end


endmodule

//---------------------------------------------------------------------- SHIFTER

module shifter(

    input [31:0] in,

    output reg [31:0] out
    
);

    always @(in)
    begin
        out = in*4;
    end

endmodule

//---------------------------------------------------------------------- COMPARE

module compare(

    input [31:0] a,
    input [31:0] b,

    output reg out
    
);

    always @(a or b)
    begin
        if(a == b) out = 1;
        else out = 0;
    end

endmodule

//---------------------------------------------------------------------- REG32

module reg32(

    input [31:0] datain,
    input clk,

    output reg [31:0] dataout

);

    always @(posedge clk)
    begin
        dataout = datain;
    end

endmodule

//---------------------------------------------------------------------- REG32_RES

module reg32_res(

    input [31:0] datain,
    input reset,
    input clk,

    output reg [31:0] dataout

);

    always @(posedge clk or reset)
    begin
        if(reset == 1)
            dataout = 0;
        else
            dataout = datain;
    end

endmodule

//---------------------------------------------------------------------- ALU

module alu(

    input wire [31:0] SrcA,
    input wire [31:0] SrcB,
    input wire [3:0] ALUControl,

    output reg zero,
    output reg signed [31:0] ALUResult

);

    function automatic [7:0] satsum; //fce na saturovaný součet dvou 8 bitových čísel
        input [7:0] a, b;
        begin
            if (a + b > 255) //Výsledek je mimo rozsah
                satsum = 255;
            else 
                satsum = a + b; //vysledek je v 8b rozsahu
        end
    endfunction

    always @(SrcA or SrcB or ALUControl) //ALURESULT
    begin
        case(ALUControl)
            // ADD
            'b0010:     ALUResult = SrcA + SrcB; 
            // SUB
            'b0110:     ALUResult = SrcA - SrcB; 
            // AND
            'b0000:     ALUResult = SrcA & SrcB; 
            // OR
            'b0001:     ALUResult = SrcA | SrcB; 
            // XOR
            'b0011:     ALUResult = SrcA ^ SrcB; 
            // STL
            'b0111:     ALUResult = SrcA < SrcB; 
            //čtyři neznamánkové součty
            'b1000:     begin                  
                            ALUResult[ 7: 0] = SrcA[ 7: 0] + SrcB[ 7: 0];
                            ALUResult[15: 8] = SrcA[15: 8] + SrcB[15: 8];
                            ALUResult[23:16] = SrcA[23:16] + SrcB[23:16];
                            ALUResult[31:24] = SrcA[31:24] + SrcB[31:24];
                        end
            //čtyři neznamánkové saturované součty
            'b1001:     begin
                            ALUResult[ 7: 0] = satsum(SrcA[ 7: 0], SrcB[ 7: 0]);
                            ALUResult[15: 8] = satsum(SrcA[15: 8], SrcB[15: 8]);
                            ALUResult[23:16] = satsum(SrcA[23:16], SrcB[23:16]);
                            ALUResult[31:24] = satsum(SrcA[31:24], SrcB[31:24]);
                        end
            //ERROR
            default:    ALUResult = 'h69; 
        endcase
    end

    always @(ALUResult) //ZERO
    begin
        if(ALUResult == 0)
            zero = 1;
        else
            zero = 0;
    end

endmodule

//---------------------------------------------------------------------- CTLUNIT

module ctlUnit(

    input [5:0] opcode,
    input [5:0] funct,
    input [4:0] shamt,

    output regWrite,
    output regDst,
    output aluSrc,
    output branch,
    output memWrite,
    output memToReg,
    output PCSrcJal,
    output PCSrcJr,
    output [3:0] ALUControl

);

    wire [1:0] aluOP_wire;

    mainDec mainDec_mod(opcode, regWrite, regDst, aluSrc, aluOP_wire, branch, memWrite, memToReg, PCSrcJal, PCSrcJr);
    aluDec aluDec_mod(aluOP_wire, funct, shamt, ALUControl);
    
endmodule

//************************ CTLUNIT.MAINDEC

module mainDec(

    input [5:0] opcode,

    output regWrite,
    output regDst,
    output aluSrc,
    output [1:0] aluOP,
    output branch,
    output memWrite,
    output memToReg,
    output PCSrcJal,
    output PCSrcJr

);

    reg [9:0] outputs; // regWrite, regDst, aluSrc, [1:0] aluOP, branch, memWrite, memToReg, PCSrcJal, PCSrcJr

    always @(opcode)
    begin
        case(opcode)

            'b000000:   outputs = 'b1101000000; //R TYP
            'b100011:   outputs = 'b1010000100; //lw
            'b101011:   outputs = 'b0X10001X00; //sw
            'b000100:   outputs = 'b0X00110X00; //beq
            'b001000:   outputs = 'b1010000000; //addi
            'b000011:   outputs = 'b1XXXXX0X10; //jal
            'b000111:   outputs = 'b0XXXXX0X01; //jr
            'b011111:   outputs = 'b1101100000; //addu
            default:    outputs = 'b0000000000; //ERROR

        endcase
    end

    assign regWrite     = outputs[9];
    assign regDst       = outputs[8];
    assign aluSrc       = outputs[7];
    assign aluOP[1:0]   = outputs[6:5];
    assign branch       = outputs[4];
    assign memWrite     = outputs[3];
    assign memToReg     = outputs[2];
    assign PCSrcJal     = outputs[1];
    assign PCSrcJr      = outputs[0];

endmodule

//************************ CTLUNIT.ALUDEC

module aluDec(

    input [1:0] ALUOp,
    input [5:0] funct,
    input [4:0] shamt,

    output reg [3:0] ALUControl

);
    
    always @(ALUOp or funct or shamt)
    begin
        case(ALUOp)

            'b00:       ALUControl = 'b0010; //Addition
            'b01:       ALUControl = 'b0110; //Subtraction

            'b10:       begin
                            case(funct)

                                'b100000:   ALUControl = 'b0010; //Additon
                                'b100010:   ALUControl = 'b0110; //Subtraction
                                'b100100:   ALUControl = 'b0000; //And
                                'b100101:   ALUControl = 'b0001; //or
                                'b101010:   ALUControl = 'b0111; //set less than

                            endcase
                        end

            'b11:       begin
                            case(funct)
                                
                                'b010000:   begin
                                                case(shamt)

                                                    'b00000:    ALUControl = 'b1000; //Additon byte after byte
                                                    'b00100:    ALUControl = 'b1001; //saturated addition byte after byte

                                                endcase
                                            end
                            endcase   
                        end     

            default:    ALUControl = 'b1111; //ERROR
            
        endcase
    end

endmodule

//---------------------------------------------------------------------- REGFILE

module regFile(

    input [4:0] A1,
    input [4:0] A2,
    input [4:0] A3,
    input [31:0] WD3,
    input WE3,
    input clk,
    
    output reg [31:0] RD1,
    output reg [31:0] RD2

);

    reg [31:0] rf[31:0];

    always @(A1) //Výpis registrů pomocí A1
    begin
        if(A1 == 0)
            RD1 = 'h0000_0000;
        else
            RD1 = rf[A1];
    end

    always @(A2) //Výpis registrů pomocí A2
    begin
        if(A2 == 0)
            RD2 = 'h0000_0000;
        else
            RD2 = rf[A2];
    end

    always @(posedge clk)
    begin
        if(WE3 == 1)
            rf[A3] = WD3;
        else ;
    end
    
endmodule

//---------------------------------------------------------------------- PCMUX

module pcMux(

    input [31:0] jr,
    input [31:0] jal, 
    input [31:0] beq,
    input [31:0] zero, 

    input [0:0] pcSrcBeq,
    input [0:0] pcSrcJr,
    input [0:0] pcSrcJal,

    output reg [31:0] y
    
);
  
    always @(jr or jal or beq or zero or pcSrcBeq or pcSrcJr or pcSrcJal)
    begin
        
        if(jr == 1) y = pcSrcJr;
        else if (jal == 1) y = pcSrcJal;
        else if (beq == 1) y = pcSrcBeq;
        else y = zero;

    end

endmodule

//----------------------------------------------------------------------