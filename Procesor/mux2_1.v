module mux2_1(

    input [31:0] d0,
    input [31:0] d1, 
    input select,

    output reg [31:0] y
    
);

    always @(select or d0 or d1)
    begin
        if(select == 0)
            y = d0;
        else
            y = d1;
    end

endmodule