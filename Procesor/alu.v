module alu(

    input wire [31:0] SrcA,
    input wire [31:0] SrcB,
    input wire [3:0] ALUControl,

    output reg zero,
    output reg signed [31:0] ALUResult

);

    function automatic [7:0] satsum; //fce na saturovaný součet dvou 8 bitových čísel
        input [7:0] a, b;
        begin
            if (a + b > 255) //Výsledek je mimo rozsah
                satsum = 255;
            else 
                satsum = a + b; //vysledek je v 8b rozsahu
        end
    endfunction

    always @(SrcA or SrcB or ALUControl) //ALURESULT
    begin
        case(ALUControl)
            // ADD
            'b0010:     ALUResult = SrcA + SrcB; 
            // SUB
            'b0110:     ALUResult = SrcA - SrcB; 
            // AND
            'b0000:     ALUResult = SrcA & SrcB; 
            // OR
            'b0001:     ALUResult = SrcA | SrcB; 
            // XOR
            'b0011:     ALUResult = SrcA ^ SrcB; 
            // STL
            'b0111:     ALUResult = SrcA < SrcB; 
            //čtyři neznamánkové součty
            'b1000:     begin                  
                            ALUResult[ 7: 0] = SrcA[ 7: 0] + SrcB[ 7: 0];
                            ALUResult[15: 8] = SrcA[15: 8] + SrcB[15: 8];
                            ALUResult[23:16] = SrcA[23:16] + SrcB[23:16];
                            ALUResult[31:24] = SrcA[31:24] + SrcB[31:24];
                        end
            //čtyři neznamánkové saturované součty
            'b1001:     begin
                            ALUResult[ 7: 0] = satsum(SrcA[ 7: 0], SrcB[ 7: 0]);
                            ALUResult[15: 8] = satsum(SrcA[15: 8], SrcB[15: 8]);
                            ALUResult[23:16] = satsum(SrcA[23:16], SrcB[23:16]);
                            ALUResult[31:24] = satsum(SrcA[31:24], SrcB[31:24]);
                        end
            //ERROR
            default:    ALUResult = 'h69; 
        endcase
    end

    always @(ALUResult) //ZERO
    begin
        if(ALUResult == 0)
            zero = 1;
        else
            zero = 0;
    end

endmodule